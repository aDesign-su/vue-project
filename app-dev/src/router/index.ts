import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import HelloWorld from '@/components/HelloWorld.vue';
import About from '@/views/AboutView.vue';
import General from '@/components/General.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HelloWorld
  },
  {
    path: '/about',
    name: 'AboutView',
    component: About
  },
  {
    path: '/general',
    name: 'General',
    component: General
  }
];

const router = new VueRouter({
  mode: 'history', // Включение режима "history"
  routes
});
export default router;
